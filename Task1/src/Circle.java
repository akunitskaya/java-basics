/*
Вычислить длину окружности и площадь круга одного и того же заданного радиуса
R.

Длина окружности = 2πr,
Площадь окружности = πr2

*/

import java.util.Scanner;

public class Circle {

    public static void main(String[] args) {

        double radius = readRadiusFromKeyboard();
        double circleLength = calculateCircleLength(radius);
        double circleSquare = calculateCircleSquare(radius);

        System.out.println("Circle length is: " + circleLength);
        System.out.println("Circle square is: " + circleSquare);

    }

    public static double readRadiusFromKeyboard() {

        Scanner sc = new Scanner(System.in);

        System.out.println("Enter a double value for radius ");

        while (!sc.hasNextDouble()) {
            System.out.println("The entered value is not double. Enter double value for r");
            sc.next();
        }

        return sc.nextDouble();
    }

    public static double calculateCircleLength(double radius){

        return 2 * Math.PI * radius;
    }

    public static double calculateCircleSquare(double radius){

        return Math.PI * Math.pow (radius, 2);
    }
}