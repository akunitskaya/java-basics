/*
Вычислить значение выражения по формуле
(все переменные принимают действительные значения)
*/

import java.util.Scanner;

public class Formula {

    public static void main(String[] args) {
        int a, b, c;

        a = readNumberFromKeyboard("a");
        b = readNumberFromKeyboard("b");
        c = readNumberFromKeyboard("c");

        double result = calculateFormula(a, b, c);

        System.out.println("Here is the result: " + result);
    }

    public static int readNumberFromKeyboard(String numberName){

        Scanner sca = new Scanner(System.in);

        System.out.println("Enter int " + numberName);

        while (!sca.hasNextInt())  {
            System.out.println("The entered value is not int. Enter int " + numberName);
            sca.next();
        }

        return sca.nextInt();
    }

    public static double calculateFormula(int a, int b, int c) {

        return (b + (Math.sqrt(b * b + 4 * a * c) / 2 * a)) - (a * a * a * c + b);

    }
}