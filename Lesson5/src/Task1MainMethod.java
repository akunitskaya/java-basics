import java.util.*;

public class Task1MainMethod {

    public static final int RANDOM_BOUND = 50;

    public static void main(String[] args) {

        System.out.println("Enter array size k: ");
        int k = enterNumberFromKeyboard();

        ArrayList<Decimal> decimalsArray = new ArrayList<>();
        fillOutDecimalsArray(decimalsArray, k);

        System.out.println("Decimals: ");
        for (Decimal decimal:decimalsArray) {
            System.out.println(decimal.toString());
        }
    }

    public static int enterNumberFromKeyboard() {
        Scanner sc = new Scanner(System.in);
        return sc.nextInt();
    }

    public static int getRandomNumber() {
        Random rand = new Random();
        return rand.nextInt(RANDOM_BOUND);
    }

    public static void fillOutDecimalsArray(ArrayList decimals, int k) {
        for (int i = 0; i < k; i++) {
            int n = getRandomNumber();
            int m = getRandomNumber();
            Decimal mn = new Decimal(m, n);
            decimals.add(mn);
        }
    }
}
