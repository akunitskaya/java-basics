
public class Decimal {

    private int m; //nominator
    private int n; //denominator

    public Decimal(int m, int n){
        if(n == 0){
            n++;
        }
        this.m = m;
        this.n = n;
    }

    @Override
    public String toString() {
        return String.valueOf(m) + "/" + String.valueOf(n);
    }
}