/*
В массив A [N] занесены натуральные числа. Найти сумму тех элементов, которые
кратны данному К
 */

import java.util.Arrays;
import java.util.Random;

public class Task2 {
    public static void main(String[] args) {
        int n = getN();
        int k = getK();
        int sum;

        int[] array;

        array = getArrayWithRandomValues(n);

        sum = countSumOfValuesDividedByK(array, k);

        System.out.println("Array: " + Arrays.toString(array));
        System.out.println("N: " + n);
        System.out.println("K: " + k);
        System.out.println("The sum of figures from the array exactly divisible by K: " + sum);
    }

    public static int getN(){
        Random random = new Random();
        return random.nextInt(15);
    }

    public static int getK(){
        Random random = new Random();
        int k = 0;
        while(k == 0){
           k = random.nextInt(5);
        }
        return k;
    }

    public static int[] getArrayWithRandomValues(int arraySize){
        int[] array = new int[arraySize];
        Random random = new Random();
        for(int i = 0; i < array.length; i++){
            array[i] = random.nextInt(30);
        }
        return array;
    }

    public static int countSumOfValuesDividedByK(int[] array, int k) {
        if(array.length == 0){
            return 0;

        } else {
            int sum = 0;
            for (int i = 0; i < array.length; i++) {

                if (array[i] % k == 0) {
                    sum = sum + array[i];
                }
            }
            return sum;
        }
    }
}