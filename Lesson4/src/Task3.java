/*
Дана последовательность действительных чисел а1 а2 ,..., аn . Выяснить, будет ли она
возрастающей
 */

import java.util.Arrays;
import java.util.Random;

public class Task3 {
    public static void main(String[] args) {
        int[] arrayShort = new int[3];
        int[] arrayLong = new int[7];
        boolean result;

        fillOutArrayWithValues(arrayShort);
        fillOutArrayWithValues(arrayLong);

        result = isArrayAscending(arrayShort);
        System.out.println("Is array " + Arrays.toString(arrayShort) + " ascending? > " + result);

        result = isArrayAscending(arrayLong);
        System.out.println("Is array " + Arrays.toString(arrayLong) + " ascending? > " + result);

    }

    public static void fillOutArrayWithValues(int[] array){
        Random random = new Random();
        for(int i = 0; i < array.length; i++){
            array[i] = random.nextInt(10);
        }
    }

    public static boolean isArrayAscending(int[] array){


        for(int i = 0; i < array.length-1; i++){
            if(array[i] > array[i+1]) {
                return false;
            }
        }
        return true;
    }
}