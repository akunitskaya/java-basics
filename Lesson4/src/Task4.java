/*
В целочисленной последовательности есть нулевые элементы. Создать массив из
номеров этих элементов.
 */

import java.util.ArrayList;
import java.util.Arrays;

public class Task4 {
    public static void main(String[] args) {
        int[] array = {0, 1, 0, 4, 7, 0, 5, 2, 0, 0};
        ArrayList <Integer> arrayOfIndexes = makeArrayOfZeroIndexElements(array);

        System.out.println("Array: " + Arrays.toString(array));
        System.out.println("Indexes of elements from the array with 0 value: " + arrayOfIndexes);
    }

    public static ArrayList <Integer> makeArrayOfZeroIndexElements(int[] array){
        ArrayList <Integer> arrayWithZeros = new ArrayList<>();

        for(int i = 0; i < array.length; i++){
            if(array[i] == 0){
                arrayWithZeros.add(i);
            }
        }
        return arrayWithZeros;
    }
}