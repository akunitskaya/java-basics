import com.epam.lesson9.book.children.ChildrensBook;
import com.epam.lesson9.book.other.OtherBook;
import com.epam.lesson9.book.student.EngishStudentBook;
import com.epam.lesson9.book.student.EnglishLevel;
import com.epam.lesson9.book.student.MathematicsPart;
import com.epam.lesson9.book.student.MathematicsStudentBook;

/*
Создайте иерархию печатных изданий для библиотеки
 */
public class Main {
    public static void main(String[] args) {
        MathematicsStudentBook mathBook = new MathematicsStudentBook("Algebra for 6th grade", 2018, "Bell", 6,  MathematicsPart.ALGEBRA);

        EngishStudentBook englishBook = new EngishStudentBook();
        englishBook.setTitle("English grammar");
        englishBook.setPublicationYear(2008);
        englishBook.setAuthor("Murphy");
        englishBook.setGrade(9);
        englishBook.setProficiency(EnglishLevel.INTERMEDIATE);

        ChildrensBook childrensBook = new ChildrensBook("First steps", 2016, "Jonson", 5);
        OtherBook otherBook = new OtherBook("Cook everything", 2000, "Smitt", "Cooking");

        System.out.println(mathBook);
        System.out.println(englishBook);
        System.out.println(childrensBook);
        System.out.println(otherBook);
    }
}
