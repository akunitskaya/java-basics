package com.epam.lesson9.book.other;

import com.epam.lesson9.book.Book;

public class OtherBook extends Book {
    private String topic;

    public OtherBook(String title, int publicationYear, String author, String topic) {
        super(title, publicationYear, author);
        this.topic = topic;
    }

    public OtherBook() {
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    @Override
    public String toString() {
        return "Book{" +
                "title='" + getTitle() + '\'' +
                ", publicationYear=" + getPublicationYear() +
                ", author='" + getAuthor() + '\'' +
                '}' + "OtherBook{" +
                "topic='" + topic + '\'' +
                '}';
    }
}
