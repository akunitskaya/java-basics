package com.epam.lesson9.book.student;

public enum EnglishLevel {
    BASIC, INTERMEDIATE, ADVANCED, PROFICIENT
}
