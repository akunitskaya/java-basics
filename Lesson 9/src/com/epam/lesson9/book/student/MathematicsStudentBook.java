package com.epam.lesson9.book.student;

public class MathematicsStudentBook extends StudentBook {
    private MathematicsPart part;

    public MathematicsStudentBook(String title, int publicationYear, String author, int grade, MathematicsPart part) {
        super(title, publicationYear, author, grade);
        this.part = part;
    }

    public MathematicsStudentBook() {
    }

    public MathematicsPart getPart() {
        return part;
    }

    public void setPart(MathematicsPart part) {
        this.part = part;
    }

    @Override
    public String toString() {
        return "Book{" +
                "title='" + getTitle() + '\'' +
                ", publicationYear=" + getPublicationYear() +
                ", author='" + getAuthor() + '\'' +
                '}' +
                "StudentBook{" +
                "grade=" + getGrade() +
                '}'+
                "MathematicsStudentBook{" +
                "part=" + part +
                '}';
    }
}
