package com.epam.lesson9.book.student;

public class EngishStudentBook extends StudentBook {
    private EnglishLevel proficiency;

    public EngishStudentBook(String title, int publicationYear, String author, int grade, EnglishLevel proficiency) {
        super(title, publicationYear, author, grade);
        this.proficiency = proficiency;
    }

    public EngishStudentBook() {
    }

    public EnglishLevel getProficiency() {
        return proficiency;
    }

    public void setProficiency(EnglishLevel proficiency) {
        this.proficiency = proficiency;
    }

    @Override
    public String toString() {
        return "Book{" +
                "title='" + getTitle() + '\'' +
                ", publicationYear=" + getPublicationYear() +
                ", author='" + getAuthor() + '\'' +
                '}' +
                "StudentBook{" +
                "grade=" + getGrade() +
                '}' +
                "EngishStudentBook{" +
                "proficiency=" + proficiency +
                '}';
    }
}
