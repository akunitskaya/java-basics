package com.epam.lesson9.book.student;

public enum MathematicsPart {
    ALGEBRA, GEOMETRY
}
