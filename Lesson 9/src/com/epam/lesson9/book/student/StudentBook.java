package com.epam.lesson9.book.student;

import com.epam.lesson9.book.Book;

public class StudentBook extends Book {
    private int grade;

    public StudentBook(String title, int publicationYear, String author, int grade) {
        super(title, publicationYear, author);
        this.grade = grade;
    }

    public StudentBook() {
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    @Override
    public String toString() {
        return "Book{" +
                "title='" + getTitle() + '\'' +
                ", publicationYear=" + getPublicationYear() +
                ", author='" + getAuthor() + '\'' +
                '}' +
                "StudentBook{" +
                "grade=" + grade +
                '}';
    }
}
