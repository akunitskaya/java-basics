package com.epam.lesson9.book.children;

import com.epam.lesson9.book.Book;

public class ChildrensBook extends Book {
    private int age;

    public ChildrensBook(String title, int publicationYear, String author, int age) {
        super(title, publicationYear, author);
        this.age = age;
    }

    public ChildrensBook() {
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Book{" +
                "title='" + getTitle() + '\'' +
                ", publicationYear=" + getPublicationYear() +
                ", author='" + getAuthor() + '\'' +
                '}' +
                "ChildrensBook{" +
                "age=" + age +
                '}';
    }
}
