import java.util.Scanner;

public class Task3 {

    public static void main(String[] args) {

        double fx;
        double x;

        x = readXFromKeyboard();
        fx = countFX(x);

        System.out.println("F(x) is: " + fx);
    }

    public static double readXFromKeyboard(){
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter x: ");
        return sc.nextDouble();
    }

    public static double countFX(double x) {
        if (x <= -3) {
            return 9;
        } else if (x > 3) {
            return 1 / (Math.pow(x, 2) + 1);
        }else{
            return 0;
        }
    }
}