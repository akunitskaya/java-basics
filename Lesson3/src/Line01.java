/*
Примените при решении задач из листингов l3-5, l3-6, l3-7, l3-8
процедурную декомпозицию – оформите решения как совокупность
взаимоствязанных методов, каждый из которых выполняет одно действие.

22
Листинг 13-8.
*/

import java.util.Scanner;

public class Line01 {
    public static void main(String[] args) {
        int number = 0;
        int digit = 0;

        number = readNumberFromKeyboard();

        while (number !=0){
            digit = getLastDigit(number);
            if (isEven(digit)){
                System.out.println("В числе есть четная цифра");
                return;
            }else{
            number = removeLastDigit(number);
            }
        }

        System.out.println("В числе нет четных цифр");
    }

    public static int readNumberFromKeyboard() {

        System.out.print("Введите число: ");
        Scanner sc = new Scanner(System.in);
        return sc.nextInt();
    }

    public static int getLastDigit(int number) {
       return number % 10;
    }

    public static boolean isEven(int digit) {
        if (digit % 2 == 0) {
            return true;
        }else{
        return false;
        }
    }

    public static int removeLastDigit (int number) {
       return number / 10;
    }
}