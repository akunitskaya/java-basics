/*
Примените при решении задач из листингов l3-5, l3-6, l3-7, l3-8
процедурную декомпозицию – оформите решения как совокупность
взаимоствязанных методов, каждый из которых выполняет одно действие.

Листинг 13-5.
*/

import java.util.Scanner;

public class MinMaxString {
    public static void main(String[] args) {
       String[] array = new String[10];

       enterArrayFromKeyboard(array);

       String max = findMax(array);
       String min = findMin(array);

       System.out.println("max string = " + max + " length=" + max.length());
       System.out.println("min string = " + min + " length=" + min.length());
    }

   public static void enterArrayFromKeyboard(String[] array){
       Scanner sc = new Scanner(System.in);
       for (int i = 0; i < array.length; i++){
           System.out.print(">");
           array[i] = sc.nextLine();
       }
   }

    public static String findMax(String[] array) {
        String max = array[0];

        for (int i = 0; i < array.length; i++) {
            if (max.length() < array[i].length()) {
                max = array[i];
            }
        }
        return max;
    }

    public static String findMin(String[] array){
        String min = array[0];

        for (int i = 0; i < array.length; i++) {
            if (min.length() > array[i].length()) {
                min = array[i];
            }
        }
        return min;
    }
}