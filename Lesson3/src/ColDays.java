/*
Примените при решении задач из листингов l3-5, l3-6, l3-7, l3-8
процедурную декомпозицию – оформите решения как совокупность
взаимоствязанных методов, каждый из которых выполняет одно действие.

Листинг 13-7.
*/

import java.util.Scanner;

public class ColDays {
    public static void main(String[] args) {

        int year = 0, month = 0, col_day = 0;

        year = readYearFromKeyboard();
        month = readMonthFromKeyboard();
        col_day = countDaysInMonth(year, month);

        System.out.println("Количество дней равно - " + col_day);
    }

    public static int readYearFromKeyboard() {
        System.out.print("Введите год: ");
        Scanner sc = new Scanner(System.in);
        return sc.nextInt();
    }

    public static int readMonthFromKeyboard() {
        System.out.print("Введите номер месяца: ");
        Scanner sc = new Scanner(System.in);
        return sc.nextInt();
    }

    public static int countDaysInMonth(int year, int month) {
        int col_day = 0;
        switch (month) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                return 31;
            case 2:
                if (year % 4 == 0) {
                    return 29;
                } else {
                    return 28;
                }
            case 4:
            case 6:
            case 9:
            case 11:
                return 30;
        }
        return col_day;
    }
}