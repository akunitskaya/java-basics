/*
Примените при решении задач из листингов l3-5, l3-6, l3-7, l3-8
процедурную декомпозицию – оформите решения как совокупность
взаимоствязанных методов, каждый из которых выполняет одно действие.

Листинг 13-6.
*/

import java.util.Scanner;

public class LastDigit {
    public static void main(String[] args) {
        int number = 0;
        int poslZifra;
        int poslZifraKv;

        number = readNumberFromKeyboard();
        poslZifra = getLastNumberDigit(number);
        poslZifraKv = getLastSquareDigit(poslZifra);

        System.out.println("Последняя цифра квадрата числа " + number + " равняется " + poslZifraKv);
    }

    public static int readNumberFromKeyboard(){
        System.out.print("Введите число: ");
        Scanner sc = new Scanner(System.in);
        return  sc.nextInt();
    }

    public static int getLastNumberDigit(int number){
        return number % 10;
    }

    public static int getLastSquareDigit(int poslZifra){
        int poslZifraKv = 0;
        switch (poslZifra) {
            case 0:
                poslZifraKv = 0;
                break;
            case 1:
                poslZifraKv = 1;
                break;
            case 2:
                poslZifraKv = 4;
                break;
            case 3:
                poslZifraKv = 9;
                break;
            case 4:
                poslZifraKv = 6;
                break;
            case 5:
                poslZifraKv = 5;
                break;
            case 6:
                poslZifraKv = 6;
                break;
            case 7:
                poslZifraKv = 9;
                break;
            case 8:
                poslZifraKv = 4;
                break;
            case 9:
                poslZifraKv = 1;
                break;
            default:
                System.out.println("Что-то не то с программой.");
        }
        return poslZifraKv;
    }
}
