import java.util.ArrayList;
import java.util.Scanner;

public class Task2 {

    public static void main(String[] args) {
        int h, a, b, x;

        double fx;

        h = readFromKeyboard("h");
        a = readFromKeyboard("a");
        b = readFromKeyboard("b");

        calculateFxOnAB(a, b, h);
    }

    public static int readFromKeyboard(String variableName){
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter " + variableName + ": ");
        return sc.nextInt();
    }

    public static void calculateFxOnAB(int a, int b, int h){
        double fx;
        int x = a;
        for (int i = a; i < b; i = i+h){
            fx = 2 * Math.tan(x / 2) + 1;
            x = x + h;
            System.out.println("Fx = " + fx + ", x = " + x + " on a = " + a + ", b = " + b);
        }
    }
}