package com.epam.group;

import com.epam.student.Student;

import java.util.List;

public class Group {
    private List<Student> studentsInGroup;
    private int groupAverageGrade;

    public Group(List<Student> group) {
        this.studentsInGroup = group;
        recalculateAverageGroupGrade();
    }

    public Group() {
    }

    public List<Student> getStudentsInGroup() {
        return studentsInGroup;
    }

    public void setStudentsInGroup(List<Student> studentsInGroup) {
        this.studentsInGroup = studentsInGroup;
        recalculateAverageGroupGrade();
    }

    public int getGroupAverageGrade() {
        return groupAverageGrade;
    }

    private void recalculateAverageGroupGrade() {
        int averageGrade = 0;

        for (Student student : studentsInGroup) {
            averageGrade = averageGrade + student.getAverageGrade();
        }

        this.groupAverageGrade = averageGrade / studentsInGroup.size();
    }
}
