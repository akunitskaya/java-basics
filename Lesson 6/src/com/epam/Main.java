package com.epam;

/*
Оценки,полученные студентами за сессию являются атрибутами класса Student
Определить:
а) средний балл учебной группы (класс Group)
б) средний балл каждого студента
в) число отличников
г) количество студентов, имеющих 2
 */

import com.epam.group.Group;
import com.epam.student.Student;

import java.util.ArrayList;
import java.util.List;


public class Main {

    public static final int MAX_GRADE = 5;
    public static final int MIN_GRADE = 2;

    public static void main(String[] args) {

        Student john = new Student("John");
        List<Integer> johnGrades = addGradesToList(5, 3, 5, 2);
        john.setGrade(johnGrades);

        Student mark = new Student("Mark");
        List<Integer> markGrades = addGradesToList(2, 4);
        mark.setGrade(markGrades);

        Student paul = new Student("Paul");
        List<Integer> paulGrades = addGradesToList(5, 5, 4);
        paul.setGrade(paulGrades);

        Group group = new Group(addStudentsToGroup(john, mark, paul));
        printAverageStudentGrade(group);

        System.out.println("Group average grade = " + group.getGroupAverageGrade());

        int goodStudentsInGroup = getNumberOfGoodStudents(group);
        System.out.println("There are " + goodStudentsInGroup + " students with max grade");

        int badStudentsInGroup = getNumberOfBadStudents(group);
        System.out.println("There are " + badStudentsInGroup + " students with min grade");
    }

    public static List<Integer> addGradesToList(int... grade) {
        List<Integer> grades = new ArrayList<>();
        for (int g : grade) {
            grades.add(g);
        }
        return grades;
    }

    public static List<Student> addStudentsToGroup(Student... student) {
        List<Student> group = new ArrayList<>();
        for (Student person : student) {
            group.add(person);
        }
        return group;
    }

    public static void printAverageStudentGrade(Group group) {
        for (Student student : group.getStudentsInGroup()) {
            System.out.println(student.getName() + "'s average grade = " + student.getAverageGrade());
        }

    }

    public static int getNumberOfGoodStudents(Group group) {
        int goodStudentsNumber = 0;
        for (Student student : group.getStudentsInGroup()) {
            for (Integer grade : student.getGrade()) {
                if (grade == MAX_GRADE) {
                    goodStudentsNumber++;
                    break;
                }
            }
        }
        return goodStudentsNumber;
    }

    public static int getNumberOfBadStudents(Group group) {
        int badStudentsNumber = 0;
        for (Student student : group.getStudentsInGroup()) {
            for (Integer grade : student.getGrade()) {
                if (grade == MIN_GRADE) {
                    badStudentsNumber++;
                    break;
                }
            }
        }
        return badStudentsNumber;
    }
}
