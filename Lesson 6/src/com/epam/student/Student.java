package com.epam.student;

import java.util.List;

public class Student {
    private String name;
    private List<Integer> grade;
    private int averageGrade;

    public Student(String name) {
        this.name = name;
    }

    public Student() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Integer> getGrade() {
        return grade;
    }

    public void setGrade(List<Integer> grade) {
        this.grade = grade;
        recalculateAverageStudentGrade();

    }

    public int getAverageGrade() {
        return averageGrade;
    }

    private void recalculateAverageStudentGrade() {
        int averageGrade = 0;

        for (Integer grade : this.grade) {
            averageGrade = averageGrade + grade;
        }
        this.averageGrade = averageGrade / grade.size();
    }
}
