package com.epam.library.service.comparator;

import com.epam.library.entity.Edition;

public class PublicationYearComparator extends EditionComparator {
    @Override
    public int compare(Edition o1, Edition o2) {
        double publicationYear1 = o1.getPublicationYear();
        double publicationYear2 = o2.getPublicationYear();

        if (publicationYear1 < publicationYear2) {
            return -1;
        } else if (publicationYear1 == publicationYear2) {
            return 0;
        } else {
            return 1;
        }
    }
}
