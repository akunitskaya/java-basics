package com.epam.library.service.comparator;

import com.epam.library.entity.Edition;

public class PriceComparator extends EditionComparator {
    @Override
    public int compare(Edition o1, Edition o2) {
        double price1 = o1.getPrice();
        double price2 = o2.getPrice();

        if (price1 < price2) {
            return -1;
        } else if (price1 == price2) {
            return 0;
        } else {
            return 1;
        }
    }
}
