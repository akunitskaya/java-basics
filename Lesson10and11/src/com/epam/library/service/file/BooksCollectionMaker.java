package com.epam.library.service.file;

import com.epam.library.entity.Book;
import com.epam.library.entity.BookTopic;

import java.util.ArrayList;
import java.util.List;

public class BooksCollectionMaker {
    public static List<Book> getBooksCollectionFromFileContent(List<String> linesFromFile) {

        List<Book> books = new ArrayList<>();

        for (int i = 0; i < linesFromFile.size(); i++) {
            Book book = new Book();

            String[] lines = linesFromFile.get(i).split("\\;");

            double price = Double.parseDouble(lines[0]);
            int year = Integer.parseInt(lines[1]);
            String author = lines[2];
            String topicString = lines[3];
            BookTopic topic = BookTopic.valueOf(topicString);

            book.setPrice(price);
            book.setPublicationYear(year);
            book.setAuthor(author);
            book.setTopic(topic);

            books.add(book);
        }
        return books;
    }
}
