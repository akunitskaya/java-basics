package com.epam.library.service.file;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class FilesReader {

    public static String DEFAULT_PATH = "/Users/aleksandra/Documents/Projects/Java Basics/Lesson10and11/src/com/epam/library/service/file/files/books.txt";

    public static List<String> readFile(String path) throws IOException {

        BufferedReader in = null;
        List<String> lines = new ArrayList<>();

        try {
            in = new BufferedReader(new FileReader(path));


            String lineFromFile = in.readLine();

            while (lineFromFile != null) {
                lines.add(lineFromFile);
                lineFromFile = in.readLine();
            }

        } finally {
            if (in != null) in.close();
        }

        return lines;
    }
}
