package com.epam.library.service.finder;

import com.epam.library.entity.Edition;

import java.util.ArrayList;
import java.util.List;

public class FinderByPublicationYear implements Findable {
    int publicationYear;

    public FinderByPublicationYear(int publicationYear) {
        this.publicationYear = publicationYear;
    }

    @Override
    public List<Edition> find(List<Edition> editions) {
        List<Edition> yearEditions = new ArrayList<>();
        for (Edition edition : editions) {
            if (edition.getPublicationYear() == publicationYear) {
                yearEditions.add(edition);
            }
        }
        return yearEditions;
    }
}
