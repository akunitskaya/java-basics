package com.epam.library.service.finder;

import com.epam.library.entity.Edition;

import java.util.List;

public interface Findable {
    List<Edition> find(List<Edition> editions);
}
