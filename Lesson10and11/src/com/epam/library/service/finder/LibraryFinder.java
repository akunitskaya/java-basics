package com.epam.library.service.finder;

import com.epam.library.entity.Edition;
import com.epam.library.entity.Library;

import java.util.List;

public class LibraryFinder {
    public List<Edition> find(Library library, Findable matcher) {
        List<Edition> editions = library.getEditions();
        List<Edition> matchingEditions = matcher.find(editions);
        return matchingEditions;
    }
}
