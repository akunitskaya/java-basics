package com.epam.library.service.printer;

import com.epam.library.entity.Edition;

import java.util.List;

public class ListPrinter implements Printable {
    @Override
    public void print(List<Edition> editions) {
        System.out.println("Here is a list of editions: ");
        for (Edition edition : editions) {
            edition.toString();
            System.out.println("Edition: " + edition + ";");
        }
    }

    public ListPrinter(List<Edition> editions) {
        print(editions);
    }
}
