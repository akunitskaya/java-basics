package com.epam.library.service.printer;

import com.epam.library.entity.Edition;

import java.util.List;

public interface Printable {
    void print(List<Edition> editions);
}
