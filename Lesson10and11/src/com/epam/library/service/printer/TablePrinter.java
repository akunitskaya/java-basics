package com.epam.library.service.printer;

import com.epam.library.entity.Edition;

import java.util.List;

public class TablePrinter implements Printable {
    @Override
    public void print(List<Edition> editions) {
        System.out.println("Here is a table of editions: ");
        for (int i = 0; i < editions.size(); i++) {
            System.out.println("| Book No.         | " + i + "    |");
            System.out.println("| Price            | " + editions.get(i).getPrice() + " |");
            System.out.println("| Publication Year | " + editions.get(i).getPublicationYear() + " |");
            System.out.println("| Author           | " + editions.get(i).getAuthor() + "  |");
        }
    }

    public TablePrinter(List<Edition> editions) {
        print(editions);
    }
}
