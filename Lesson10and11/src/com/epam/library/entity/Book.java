package com.epam.library.entity;

public class Book extends Edition {
    private BookTopic topic;

    public Book(double price, int publicationYear, String author, BookTopic topic) {
        super(price, publicationYear, author);
        this.topic = topic;
    }

    public Book() {
    }

    public BookTopic getTopic() {
        return topic;
    }

    public void setTopic(BookTopic topic) {
        this.topic = topic;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Book book = (Book) o;

        return topic == book.topic;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (topic != null ? topic.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return super.toString() +
                "Book{" +
                "topic=" + topic +
                '}';
    }
}
