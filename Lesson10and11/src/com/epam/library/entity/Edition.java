package com.epam.library.entity;

public abstract class Edition {
    private double price;
    private int publicationYear;
    private String author;

    public Edition(double price, int publicationYear, String author) {
        this.price = price;
        this.publicationYear = publicationYear;
        this.author = author;
    }

    public Edition() {
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getPublicationYear() {
        return publicationYear;
    }

    public void setPublicationYear(int publicationYear) {
        this.publicationYear = publicationYear;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Edition edition = (Edition) o;

        if (Double.compare(edition.price, price) != 0) return false;
        if (publicationYear != edition.publicationYear) return false;
        return author != null ? author.equals(edition.author) : edition.author == null;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(price);
        result = (int) (temp ^ (temp >>> 32));
        result = 31 * result + publicationYear;
        result = 31 * result + (author != null ? author.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Edition{" +
                "price=" + price +
                ", publicationYear=" + publicationYear +
                ", author='" + author + '\'' +
                '}';
    }
}
