package com.epam.library.entity;

public class Magazine extends Edition {
    private int editionNo;

    public Magazine(double price, int publicationYear, String author, int editionNo) {
        super(price, publicationYear, author);
        this.editionNo = editionNo;
    }

    public Magazine() {
    }

    public int getEditionNo() {
        return editionNo;
    }

    public void setEditionNo(int editionNo) {
        this.editionNo = editionNo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Magazine magazine = (Magazine) o;

        return editionNo == magazine.editionNo;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + editionNo;
        return result;
    }

    @Override
    public String toString() {
        return super.toString() +
                "Magazine{" +
                "editionNo=" + editionNo +
                '}';
    }
}
