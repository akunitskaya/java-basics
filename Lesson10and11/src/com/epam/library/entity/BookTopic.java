package com.epam.library.entity;

public enum BookTopic {
    MATH,
    ENGLISH
}
