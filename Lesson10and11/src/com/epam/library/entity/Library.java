package com.epam.library.entity;

import com.epam.library.service.finder.Findable;

import java.util.ArrayList;
import java.util.List;

public class Library {
    private List<Edition> editions = new ArrayList<>();

    public List<Edition> getEditions() {
        return editions;
    }

    public void addToLibrary(Edition... edition) {
        for (Edition item : edition) {
            editions.add(item);
        }
    }

    public void removeFromLibrary(Edition... edition) {
        for (Edition item : edition) {
            editions.remove(item);
        }
    }

    public List<Edition> findEditionsByAuthor(Library library, String author) {
        List<Edition> editions = library.getEditions();
        List<Edition> authorsEditions = new ArrayList<>();

        for (Edition edition : editions) {
            if (edition.getAuthor().equals(author)) {
                authorsEditions.add(edition);
            }
        }
        return authorsEditions;
    }
}
