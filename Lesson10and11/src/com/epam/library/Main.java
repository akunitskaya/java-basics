package com.epam.library;

import com.epam.library.entity.*;
import com.epam.library.service.comparator.PriceComparator;
import com.epam.library.service.comparator.PublicationYearComparator;
import com.epam.library.service.finder.Findable;
import com.epam.library.service.finder.FinderByPublicationYear;
import com.epam.library.service.finder.LibraryFinder;
import com.epam.library.service.printer.ListPrinter;
import com.epam.library.service.printer.TablePrinter;

import java.io.IOException;
import java.util.*;

import static com.epam.library.service.file.BooksCollectionMaker.getBooksCollectionFromFileContent;
import static com.epam.library.service.file.FilesReader.DEFAULT_PATH;
import static com.epam.library.service.file.FilesReader.readFile;

/*
10. Создайте класс Library (Библиотека), содержащий список книг (или печатных изданий).
Реализуйте методы добавления, удаления и поиска книги.
Реализуйте вывод найденных книг простым списком и в виде таблицы.

11. В приложении Library добавить возможность поиска книг определенного автора
с их сортировкой 1) по стоимости, 2) по дате выхода

12. Прочитать файл, создать коллекцию книг
 */
public class Main {
    public static void main(String[] args) throws IOException, ClassNotFoundException {

        //Lesson 10
        Book bookOne = new Book(10, 2018, "Doe", BookTopic.MATH);
        Book bookTwo = new Book(15, 2000, "Doe", BookTopic.MATH);
        Book bookThree = new Book(15, 2018, "Smitt", BookTopic.ENGLISH);
        Magazine magazine = new Magazine(11, 2010, "Doe", 1);

        Library library = new Library();
        library.addToLibrary(bookOne, bookTwo, bookThree, magazine);

        List<Edition> editionsByAuthor = library.findEditionsByAuthor(library, "Doe");

        ListPrinter listPrinter = new ListPrinter(editionsByAuthor);
        System.out.println();
        TablePrinter tablePrinter = new TablePrinter(editionsByAuthor);
        System.out.println();

        //find editions of the specified year
        Findable yearMatcher = new FinderByPublicationYear(2018);
        LibraryFinder finder = new LibraryFinder();
        List<Edition> yearEditions = finder.find(library, yearMatcher);
        listPrinter.print(yearEditions);

        System.out.println();

        //Lesson 11
        //a set of the author's Editions sorted by price
        Set<Edition> sortedByPriceSet = new TreeSet<>(new PriceComparator());
        sortedByPriceSet.addAll(editionsByAuthor);
        List<Edition> sortedByPriceList = new ArrayList<>();
        sortedByPriceList.addAll(sortedByPriceSet);
        listPrinter.print(sortedByPriceList);

        System.out.println();

        //a list of the author's editions sorted by publication year
        Collections.sort(editionsByAuthor, new PublicationYearComparator());
        listPrinter.print(editionsByAuthor);

        System.out.println();

        //Lesson 12
        //get books collection from a file
        List<String> fileContent = readFile(DEFAULT_PATH);
        List<Book> booksFromFile = getBooksCollectionFromFileContent(fileContent);

        for (Book book : booksFromFile) {
            System.out.println(book.toString());
        }
    }
}