import com.epam.task1.BankAccount;
import com.epam.task2.Product;

import java.util.ArrayList;
import java.util.List;

/*
1. Создайте класс БанковскийСчет,
переопределите в нем методы equals, hashCode, toString

2. Создайте класс покупка, сформируйте покупку из нескольких товаров
*/
public class Main {
    public static void main(String[] args) {

        //1. БанковскийСчет
        System.out.println("Task 1:");

        BankAccount accountOne = new BankAccount("John", "Doe", 1000.5, 20, false);
        BankAccount accountTwo = new BankAccount("Jane", "Smitt", 500, 20, false);
        BankAccount accountThree = new BankAccount("John", "Doe", 1000.5, 20, false);

        System.out.println("Is accountOne the same as accountThree? > " + accountOne.equals(accountThree));
        System.out.println(accountOne);
        System.out.println(accountThree);
        System.out.println();

        System.out.println("Is accountOne the same as accountTwo? > " + accountOne.equals(accountTwo));
        System.out.println(accountOne);
        System.out.println(accountTwo);
        System.out.println();

        //2. Покупка
        System.out.println("Task 2:");

        Product apple = new Product("Apple", 10.5);
        Product orange = new Product("Orange", 15);
        Product carrot = new Product("Carrot", 5.5);
        Product pen = new Product("Pen", 5.5);
        Product cup = new Product("Cup", 50);

        List<Product> productList = new ArrayList<>();
        addProductsToList(productList, apple, orange, carrot, pen, cup);

        System.out.println("There are " + productList.size() + " products in the list:");
        printProductsList(productList);

    }

    public static void addProductsToList(List<Product> list, Product... product) {
        for (Product item : product) {
            list.add(item);
        }
    }

    public static void printProductsList(List<Product> list) {
        for (Product product : list) {
            System.out.println(product);
        }
    }
}
